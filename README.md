# DPG_tutorial_2024

See abstract [here](https://www.dpg-verhandlungen.de/year/2024/conference/berlin/part/tut/session/1/contribution/1).

[![Binder](https://notebooks.mpcdf.mpg.de/binder/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fsmenon%2Fdpg_tutorial_2024.git/HEAD)

## Schedule

- (16:00-00:15) Joerg - Introduction
- (16:15-17:00) Sarath - Technical set-up and hands-on workflows
- (17:00-17:35) Marvin - Introduction to pyiron for atomistics
- (17:35-18:00) Minaam - Fitting and validation of ACE potentials
- (18:00-18:15) Sarath - Calculation of phase diagrams
- (18:15-18:30) Tilmann - Conclusion
